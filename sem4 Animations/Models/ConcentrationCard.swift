//
//  ConcentrationCard.swift
//  sem4 Animations
//
//  Created by Seiks on 23/2/22.
//

import Foundation

struct ConcentrationCard {
    var isFaceUp = false
    var isMatched = false
    var identifier: Int
    
    static var identifierFactory = 0
    
    static func getUniqueIdentifier() -> Int {
         identifierFactory += 1
         return identifierFactory
    }
    
    init(){
        self.identifier = ConcentrationCard.getUniqueIdentifier()
    }
}
