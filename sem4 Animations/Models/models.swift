//
//  models.swift
//  sem4 Animations
//
//  Created by Seiks on 27/2/22.
//

import Foundation

enum TextSymbols {
    case squiggle
    case circle
    case diamond
}
enum TextColors {
    case red
    case purple
    case green
}
enum TextStyles {
    case filled
    case nonFilled
    case striped
}
