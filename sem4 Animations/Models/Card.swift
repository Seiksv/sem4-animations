
import Foundation
struct Card {
    var isFaceUp = false
    var isMatched = false
    var isSetted = false
    var identifier: Int
    var isSelected = false
    var color: TextColors
    var simbol: TextSymbols
    var textSyle: TextStyles
    var numberOfSimbolsByCard: Int
    
    static var identifierFactory = 0
    static func getUniqueIdentifier() -> Int {
         identifierFactory += 1
         return identifierFactory
    }
    
    init(color: TextColors, simbol: TextSymbols, textSyle: TextStyles, numberOfSimbolsByCard: Int){
        self.color = color
        self.simbol = simbol
        self.textSyle = textSyle
        self.numberOfSimbolsByCard = numberOfSimbolsByCard
        self.identifier = Card.getUniqueIdentifier()
    }
}
