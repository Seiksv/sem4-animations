//
//  Theme.swift
//  sem4 Animations
//
//  Created by Seiks on 23/2/22.
//

import Foundation
import UIKit

struct Theme {
    var emojisList = [String]()
    var emojiBackgroundColor: UIColor
    var emojiBackCardColor: UIColor
    var emojiFrontCardColor: UIColor
    var themeName: String
    
    init(emojisList emojisListTheme: [String], emojiDeviceBackground emojiBackgroundColor: UIColor, emojiCardBackgroundColor emojiBackCardColor: UIColor, emojiCardFrontCardColor emojiFrontCardColor: UIColor, nameOfTheme themeName: String){
        self.emojisList = emojisListTheme
        self.emojiBackgroundColor = emojiBackgroundColor
        self.emojiBackCardColor = emojiBackCardColor
        self.emojiFrontCardColor = emojiFrontCardColor
        self.themeName = themeName
    }
    
    init(){
        self.emojisList = [""]
        self.emojiBackgroundColor = UIColor.clear
        self.emojiBackCardColor = UIColor.blue
        self.emojiFrontCardColor = UIColor.orange
        self.themeName = ""
    }
}

