//
//  ConcentrationController.swift
//  sem4 Animations
//
//  Created by Seiks on 23/2/22.
//
import UIKit
import Foundation
class ConcentrationController {
    
    var cards: [ConcentrationCard] = []
    var flipCount = 0
    var indexOfOneAndOnlyFaceUpCard: Int?
    var score = 0
    var cardChecked: [Int: Int] = [:]
    var currentCardsTime = -1
    var bonusMessage = ""

    
    init(numberOfPairOfCards: Int) {
        for _ in 0..<numberOfPairOfCards {
            let card = ConcentrationCard()
            cards +=  [card, card]
        }
        cards.shuffle()
    }
    
    func chooseCard(at index: Int) {
        if !cards[index].isMatched {
            if let matchIndex = indexOfOneAndOnlyFaceUpCard, matchIndex != index {
                if cards[matchIndex].identifier == cards[index].identifier {
                    resetCardsTimeIntervalCounter()
                    cards[matchIndex].isMatched = true
                    cards[index].isMatched = true
                    score += 2
                } else {
                    if setCardChecked(at: index) || setCardChecked(at: matchIndex){
                        score -= 1
                    }
                }
                cards[index].isFaceUp = true
                indexOfOneAndOnlyFaceUpCard = nil
            } else {
                for flipDownIndex in cards.indices{
                    cards[flipDownIndex].isFaceUp = false
                }
                cards[index].isFaceUp = true
                indexOfOneAndOnlyFaceUpCard = index
                bonusMessage = ""
            }
        }
    }
    
    func setCardChecked (at index: Int) -> Bool {
        if cardChecked[index] == nil {
            cardChecked[index] = 1
            return false
        } else {
            cardChecked[index]! += 1
            return true
        }
    }
    
    func isAllCardsMatched() -> Bool {
        var cardsMatched = 0
        for card in cards {
            if card.isMatched {
                cardsMatched += 1
            }
        }
        if cardsMatched == cards.count {
            return true
        } else {
            return false
        }
    }
    
    func resetAllCards() {
        for card in cards.indices {
            cards[card].isMatched = false
            cards[card].isFaceUp = false
            cardChecked[card] = nil
        }
        indexOfOneAndOnlyFaceUpCard = nil
    }
    
    func chooseATheme(listOfThemes themes:[Int:Theme]) -> Theme {
        let randomIndex = Int(arc4random_uniform(UInt32(themes.count)))
        if themes[randomIndex] != nil{
            return themes[randomIndex]!
        } else {
            return  Theme(emojisList: ["👻","👾","💀"], emojiDeviceBackground: UIColor.blue, emojiCardBackgroundColor: UIColor.brown, emojiCardFrontCardColor: UIColor.gray, nameOfTheme: "Halloween")
        }
    }

    func setCardsTimeIntervalCounter() -> Int {
        currentCardsTime += 1
        return currentCardsTime
    }
    
    func resetCardsTimeIntervalCounter() {
        setBonusMessage()
        currentCardsTime = -1
    }
    
    func setBonusMessage()  {
        if currentCardsTime <= 2 {
            score += 1
            bonusMessage = "1 bonus points! "
        } else if currentCardsTime >= 10 {
            score -= 1
            bonusMessage = "too late -1 points! "
        } else {
            bonusMessage = ""
        }
    }
}

