    //
    //  ConcentrationViewController.swift
    //  sem4 Animations
    //
    //  Created by Seiks on 23/2/22.
    //

import UIKit

class ConcentrationViewController: UIViewController {
    
    var possibleEmojis = [Int:String]()
    var timer: Timer?
    var emojiChoices:Theme = Theme()
    lazy var game: ConcentrationController = ConcentrationController(numberOfPairOfCards: (cardButtons.count + 1) / 2)
    
    @IBOutlet weak var labelFlipCounter: UILabel!
    @IBOutlet weak var labelTiming: UILabel!
    @IBOutlet weak var labelCounter: UILabel!
    @IBOutlet weak var labelBonusMessages: UILabel!
    @IBOutlet weak var newGameButton: UIButton!
    @IBOutlet weak var themeName: UILabel!
    @IBOutlet var backgroundView: UIView!
    
    
    @IBAction func newGame(_ sender: UIButton) {
        game.resetAllCards()
        game.flipCount = 0
        game.score = 0
        game.currentCardsTime = -1
        possibleEmojis = [Int:String]()
        
        updateViewFromModel()
    }
    
    @IBOutlet var cardButtons: [UIButton]!
    
    @IBAction func touchCard(_ sender: UIButton) {
        if(game.currentCardsTime == -1){
            startTimer()
        }
        game.flipCount += 1
        if let cardNumber = cardButtons.firstIndex(of: sender) {
            game.chooseCard(at: cardNumber)
            updateViewFromModel()
        }
    }
    
    func updateViewFromModel() {
        if cardButtons != nil {
            labelBonusMessages.text = String(game.bonusMessage)
            labelFlipCounter.text = String(game.flipCount)
            labelCounter.text = String(game.score)
            themeName.text = emojiChoices.themeName
            backgroundView.backgroundColor = emojiChoices.emojiBackgroundColor
            for index in cardButtons.indices {
                let button = cardButtons[index]
                let card = game.cards[index]
                if card.isFaceUp {
                    button.setTitle(getEmoji(for: card), for: UIControl.State.normal)
                    button.backgroundColor = emojiChoices.emojiFrontCardColor
                } else {
                    button.setTitle("", for: UIControl.State.normal)
                    button.backgroundColor = card.isMatched ? UIColor.clear : emojiChoices.emojiBackCardColor
                }
            }
            if game.isAllCardsMatched() {
                newGameButton.isEnabled = true
                stopTimer()
            }
        }
    }
    
    func getEmoji(for card: ConcentrationCard) -> String {
        if possibleEmojis[card.identifier] == nil, emojiChoices.emojisList.count > 0 {
            let randomIndex = Int(arc4random_uniform(UInt32(emojiChoices.emojisList.count)))
            possibleEmojis[card.identifier] = emojiChoices.emojisList.remove(at: randomIndex)
        }
        return possibleEmojis[card.identifier] ?? "?"
    }
    
    func startTimer() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { _ in
            self.labelTiming.text = String(self.game.setCardsTimeIntervalCounter())
        }
    }
    
    func stopTimer() {
        timer?.invalidate()
    }
}
