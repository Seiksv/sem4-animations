//
//  ConcentrationThemeSelector.swift
//  sem4 Animations
//
//  Created by Seiks on 23/2/22.
//

import UIKit

class ConcentrationThemeSelector: UIViewController, UISplitViewControllerDelegate {
   
    var themesList = [
        0: Theme(emojisList: ["👻","👾","💀","👹","☠️","☂️"], emojiDeviceBackground: UIColor.blue, emojiCardBackgroundColor: .brown, emojiCardFrontCardColor: UIColor.purple, nameOfTheme: "Halloween"),
        1: Theme(emojisList: ["❤️","🧡","💛","💚","💙","💜"], emojiDeviceBackground: UIColor.red, emojiCardBackgroundColor: .black, emojiCardFrontCardColor: UIColor.red, nameOfTheme: "Hearts"),
        2: Theme(emojisList: ["😁","😎","😡","😅","🤣","🥲"], emojiDeviceBackground: UIColor.purple, emojiCardBackgroundColor: .orange, emojiCardFrontCardColor: UIColor.yellow, nameOfTheme: "Faces"),
        3: Theme(emojisList: ["✋","✊🏻","👉","🤟🏻","👍","👏"], emojiDeviceBackground: UIColor.magenta, emojiCardBackgroundColor: .blue, emojiCardFrontCardColor: UIColor.lightGray, nameOfTheme: "Hands"),
        4: Theme(emojisList: ["🍏","🍎","🍐","🍊","🍋","🍉"], emojiDeviceBackground: UIColor.systemTeal, emojiCardBackgroundColor: .blue, emojiCardFrontCardColor: UIColor.lightGray, nameOfTheme: "Fruits"),
        5: Theme(emojisList: ["🌮","🥩","🍗","🍔","🍕","🍟"], emojiDeviceBackground: UIColor.lightGray, emojiCardBackgroundColor: .blue, emojiCardFrontCardColor: UIColor.lightGray, nameOfTheme: "Foods")
    ]
    
    override func awakeFromNib() {
        splitViewController?.delegate = self
    }
    
    func splitViewController(_ splitViewController: UISplitViewController,
                             collapseSecondary secondaryViewController: UIViewController,
                             onto primaryViewController: UIViewController) -> Bool {
        if let cvc = secondaryViewController as? ConcentrationViewController {
            if cvc.emojiChoices.themeName.isEmpty {
                return true
            }
        }
        return false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if segue.identifier == "Choose Theme" {
            if let themeTag = (sender as? UIButton)?.tag, let theme = themesList[themeTag] {
                if let concentrationViewController = segue.destination as? ConcentrationViewController {
                    concentrationViewController.emojiChoices = theme
                }
            }
        }
    }
}
