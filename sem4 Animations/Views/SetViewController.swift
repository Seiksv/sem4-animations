    //
    //  ViewController.swift
    //  sem4 Animations
    //
    //  Created by Seiks on 23/2/22.
    //
import UIKit

class SetViewController: UIViewController {
    
    @IBOutlet weak var buttonAddCards: UIButton!
    @IBOutlet weak var labelCheat: UILabel!
    @IBOutlet weak var labelScore: UILabel!
    @IBOutlet weak var labelGame: UILabel!
    @IBOutlet weak var containerUIView: UIView!
    @IBOutlet weak var controllerImage: UITabBarItem!
    
    var addCadsFlag = false
    var cardViews: [UIView] = []
    var buttonSelectedCards: [Int:UIView] = [:]
    var bounds: CGRect = CGRect()
    var possibleSimbols = [Int:String]()
    var setController = SetController()
    var cellCount = 12
    var cardsInDeck = [Card]()
    var currentCards = [Card]()
    var currentSelectedCards: [Int] = []
    var grid = Grid(layout: .aspectRatio(0.3))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        controllerImage.image = UIImage(systemName: "creditcard.and.123")
        cardsInDeck = setController.generateAllRamdomCards(numberOfCards: 81)
        cardsInDeck.shuffle()
        removeAndAddCards(numberOfCads: cellCount-1)
        grid = Grid(layout: .aspectRatio(0.7), frame: containerUIView.bounds)
        setupVIew()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        containerUIView.layoutIfNeeded()
        containerUIView.setNeedsLayout()
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        containerUIView.layoutIfNeeded()
        containerUIView.setNeedsLayout()
        
        coordinator.animate(alongsideTransition: nil) { _ in
            self.grid = Grid(layout: .aspectRatio(0.7), frame: self.containerUIView.bounds)
            
            self.currentCards.shuffle()
            self.grid.cellCount = self.cellCount
            for index in 0..<self.cellCount {
                self.reorderAnimation(gridIndex: index)
            }
        }
    }
    
    @IBAction func swipeDown(_ sender: UISwipeGestureRecognizer) {
        removeAndAddCards(numberOfCads: 2)
    }
    
    @IBAction func randomizeCards(_ sender: UIRotationGestureRecognizer) {
        currentCards.shuffle()
        setupVIew()
    }
    
    @IBAction func newGame(_ sender: UIButton) {
        newGame()
    }
    
    @IBAction func buttonAddCards(_ sender: UIButton) {
        removeAndAddCards(numberOfCads: 2)
        addCadsFlag = true
        addCards()
    }
    
    private func newGame(){
        cardViews = []
        buttonSelectedCards = [:]
        bounds = CGRect()
        possibleSimbols = [Int:String]()
        cellCount = 12
        grid = Grid(layout: .aspectRatio(0.5))
        cardsInDeck = [Card]()
        currentCards = [Card]()
        currentSelectedCards = []
        setController.newGame()
        
        cardsInDeck = setController.generateAllRamdomCards(numberOfCards: 81)
        cardsInDeck.shuffle()
        removeAndAddCards(numberOfCads: cellCount-1)
        grid = Grid(layout: .aspectRatio(0.7), frame: containerUIView.bounds)
        setupVIew()
    }
    
    private func removeAndAddCards(numberOfCads: Int){
        
        if !cardsInDeck.isEmpty && cardsInDeck.count >= 3 {
            currentCards += Array(cardsInDeck[0...numberOfCads])
            cardsInDeck.removeSubrange(0...numberOfCads)
            cellCount = currentCards.count
        } else  if !cardsInDeck.isEmpty && cardsInDeck.count < 3 {
            currentCards += Array(cardsInDeck[0...cardsInDeck.count-1])
            cardsInDeck.removeSubrange(0...cardsInDeck.count-1)
            cellCount = currentCards.count
        } else {
            buttonAddCards.isEnabled = false
        }
    }
    
    func reorderAnimation(gridIndex: Int){
        var subviews = cardViews
        if let gridValue = self.grid[gridIndex] {
            
            subviews[gridIndex].subviews.forEach { (view) in
                view.removeFromSuperview()
            }
            
            UIViewPropertyAnimator.runningPropertyAnimator(
                withDuration: 0.3,
                delay: 0.1,
                options: [.curveEaseInOut],
                animations: {
                    subviews[gridIndex].frame = gridValue
                    let stackView = self.getStackView(view: subviews[gridIndex], card: self.currentCards[gridIndex])
                    stackView.alpha = 1.0
                    subviews[gridIndex].addSubview(stackView)
                })
        }
    }
    
    private func setupVIew(){
        labelScore.text = "\(setController.globalScore)"
        containerUIView.subviews.forEach { view in
            view.removeFromSuperview()
        }
        grid.cellCount = cellCount
        cardViews.removeAll()
        
        var index = 0
        var timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true){ t in
            let view = UIView()
            self.setUpUIView(view: view)
            
            self.bounds.size = CGSize(width: 92 * 0.7, height: 20)
            if let gridValue = self.grid[index]{
                view.alpha = 0.0
                self.animateView(view: view, gridValue: gridValue, cardPosition: index)
                self.cardViews.append(view)
                self.containerUIView.addSubview(view)
            }
            
            index += 1
            if index >= self.cellCount {
                t.invalidate()
            }
        }
    }
    
    
    func addCards() {
        if(self.cardsInDeck.count == 0){
            buttonAddCards.isEnabled = false
        }
        
        labelScore.text = "\(setController.globalScore)"
        grid.cellCount = cellCount
        let temp = grid.cellCount - 3
        
            //Actualiza las cartas si el deck crece o decrece
        for index in 0..<temp {
            reorderAnimation(gridIndex: index)
        }
        
        var index = temp
        _ = Timer.scheduledTimer(withTimeInterval: 0.3, repeats: true){ t in
            let view = UIView()
            self.setUpUIView(view: view)
            view.alpha = 0.0
            
            self.bounds.size = CGSize(width: 92 * 0.7, height: 20)
            if let gridValue = self.grid[index]{
                self.animateView(view: view, gridValue: gridValue, cardPosition: index)
                self.cardViews.append(view)
                self.containerUIView.addSubview(view)
            }
            
            index += 1
            if index >= self.cellCount {
                t.invalidate()
            }
        }
    }
    
    private func animateView(view: UIView, gridValue: CGRect, cardPosition: Int){
        view.frame = self.buttonAddCards.frame
        var stackView = UIView()
        UIViewPropertyAnimator.runningPropertyAnimator(
            withDuration: 0.4,
            delay: 0.2,
            options: [],
            animations: {
                view.alpha = 1.0
                view.frame = gridValue
                view.bounds.size = gridValue.size
                view.transform = CGAffineTransform.identity.rotated(by: CGFloat.pi)
                view.transform = CGAffineTransform.identity.rotated(by: CGFloat.pi * 2)
                
                stackView = self.getStackView(view: view, card: self.currentCards[cardPosition])
                stackView.alpha = 0.0
                view.addSubview(stackView)
                stackView.translatesAutoresizingMaskIntoConstraints = false
                stackView.topAnchor.constraint(equalTo: view.topAnchor, constant: CGFloat(0.0)).isActive = true
                stackView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: CGFloat(0.0)).isActive = true
                stackView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -CGFloat(0.0)).isActive = true
                stackView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -CGFloat(0.0)).isActive = true
            },
            completion: { position in
                UIView.transition(
                    with: view,
                    duration: 1,
                    options: [.transitionFlipFromLeft],
                    animations: {
                        stackView.alpha = 1.0
                    })
            })
        addCadsFlag = false
    }
    
    private func realocateCards(){
        
        grid.cellCount -= 3
        containerUIView.subviews.forEach { (view) in
            view.removeFromSuperview()
        }
        
        cardViews.removeAll()
        
        for index in 0..<grid.cellCount {
            let view = UIView()
            setUpUIView(view: view)
            self.bounds.size = CGSize(width: 92 * 0.7, height: 20)
            if let gridValue = self.grid[index] {
                view.alpha = 1.0
                view.frame = gridValue
                view.bounds.size = gridValue.size
                view.transform = CGAffineTransform.identity.rotated(by: CGFloat.pi)
                view.transform = CGAffineTransform.identity.rotated(by: CGFloat.pi * 2)
                
                let stackView = getStackView(view: view, card: self.currentCards[index])
                view.addSubview(stackView)
                stackView.translatesAutoresizingMaskIntoConstraints = false
                stackView.topAnchor.constraint(equalTo: view.topAnchor, constant: CGFloat(0.0)).isActive = true
                stackView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: CGFloat(0.0)).isActive = true
                stackView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -CGFloat(0.0)).isActive = true
                stackView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -CGFloat(0.0)).isActive = true
                stackView.alpha = 1.0
                
                self.cardViews.append(view)
                self.containerUIView.addSubview(view)
            }
        }
    }
    
    private func cardsMatchUp() {
        let firstCard = currentCards[currentSelectedCards[0]]
        let secondCard = currentCards[currentSelectedCards[1]]
        let thirdCard = currentCards[currentSelectedCards[2]]
        
        var countOfCommonsAttributes = 0
        if (firstCard.textSyle == secondCard.textSyle && secondCard.textSyle == thirdCard.textSyle && firstCard.textSyle == thirdCard.textSyle)
            ||
            (firstCard.textSyle != secondCard.textSyle && secondCard.textSyle != thirdCard.textSyle && firstCard.textSyle != thirdCard.textSyle) {
            countOfCommonsAttributes += 1
        }
        
        if (firstCard.simbol == secondCard.simbol && secondCard.simbol == thirdCard.simbol && firstCard.simbol == thirdCard.simbol)
            ||
            (firstCard.simbol != secondCard.simbol && secondCard.simbol != thirdCard.simbol && firstCard.simbol != thirdCard.simbol) {
            countOfCommonsAttributes += 1
        }
        
        if (firstCard.color == secondCard.color && secondCard.color == thirdCard.color && firstCard.color == thirdCard.color)
            ||
            (firstCard.color != secondCard.color && secondCard.color != thirdCard.color && firstCard.color != thirdCard.color) {
            countOfCommonsAttributes += 1
        }
        
        if (firstCard.numberOfSimbolsByCard == secondCard.numberOfSimbolsByCard && secondCard.numberOfSimbolsByCard == thirdCard.numberOfSimbolsByCard && firstCard.numberOfSimbolsByCard == thirdCard.numberOfSimbolsByCard)
            ||
            (firstCard.numberOfSimbolsByCard != secondCard.numberOfSimbolsByCard && secondCard.numberOfSimbolsByCard != thirdCard.numberOfSimbolsByCard && firstCard.numberOfSimbolsByCard != thirdCard.numberOfSimbolsByCard) {
            countOfCommonsAttributes += 1
        }
        
        if (countOfCommonsAttributes > 1 ){
            
            buttonSelectedCards.forEach{ (card) in
                card.value.layer.borderColor = UIColor.white.cgColor
            }
            setController.setScore(value: .match)
            animateRemoveCards()
            
        } else {
            currentSelectedCards.removeAll()
            setController.setScore(value: .mismatch)
            buttonSelectedCards.forEach{ (card) in
                card.value.layer.borderColor = UIColor.red.cgColor
            }
        }
        labelScore.text = "\(setController.globalScore)"
    }
    
    func animateRemoveCards(){
        var index = -1
        
        var timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true){ t in
            
            index += 1
            
            UIViewPropertyAnimator.runningPropertyAnimator(
                withDuration: 0.5,
                delay: 0.2,
                options: [],
                animations: {
                    if self.currentSelectedCards.count>=index {
                        
                        self.cardViews[self.currentSelectedCards[index]].alpha = 0.0
                        self.cardViews[self.currentSelectedCards[index]].frame = self.buttonAddCards.frame
                        self.cardViews[self.currentSelectedCards[index]].transform = CGAffineTransform.identity.rotated(by: CGFloat.pi)
                        self.cardViews[self.currentSelectedCards[index]].transform = CGAffineTransform.identity.rotated(by: CGFloat.pi * 2)
                    }
                }, completion: { _ in
                    if index >= 2 {
                        t.invalidate()
                        self.reloadRemovedCards()
                        return
                    }
                })
        }
    }
    
        //Evento que carga luego de haber removido las cartas
    func reloadRemovedCards(){
        var internalCounter = 0
        let counter = currentCards.count
        for index in 0..<counter {
            let view = UIView()
            setUpUIView(view: view)
            view.layer.cornerRadius = 10
            self.bounds.size = CGSize(width: 92 * 0.7, height: 20)
            if self.currentSelectedCards.contains(index) {
                
                if(self.cardsInDeck.count > 0){
                    if let gridValue = self.grid[index]{
                        self.currentCards[index] = self.cardsInDeck[0]
                        self.animateView(view: view, gridValue: gridValue, cardPosition: index)
                        self.cardViews[index] = view
                        self.containerUIView.addSubview(view)
                        self.cardsInDeck.remove(at: 0)
                        self.currentCards[index].isMatched = false
                    }
                } else {
                    self.containerUIView.willRemoveSubview(view)
                    currentCards.remove(at:  index - internalCounter)
                    buttonAddCards.isEnabled = false
                    internalCounter += 1
                    if internalCounter==3{
                        realocateCards()
                    }
                }
                self.currentSelectedCards.remove(at:  self.currentSelectedCards.firstIndex(of: index)!)
            }
        }
    }
    
    private func setUpUIView(view: UIView) {
        var gesture = UITapGestureRecognizer()
        gesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        gesture.numberOfTapsRequired = 1
        gesture.numberOfTouchesRequired = 1
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(gesture)
        view.backgroundColor = .orange
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.borderWidth = 2.0
        view.translatesAutoresizingMaskIntoConstraints = true
        view.center = self.buttonAddCards.center
        view.layer.cornerRadius = 10
    }
    
    private func getStackView(view: UIView, card: Card) -> UIStackView{
        let stackView = UIStackView()
        let numberOfSymbols = card.numberOfSimbolsByCard + 1
        stackView.alpha = 0.0
        stackView.axis  = .vertical
        stackView.distribution  =  .equalSpacing
        stackView.alignment = .center
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        for _ in 0..<numberOfSymbols {
            let customView = Shape(frame: .zero,
                                          color: card.color,
                                          symbol: card.simbol,
                                          style: card.textSyle)
            
            customView.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
            customView.heightAnchor.constraint(equalToConstant: view.frame.height/CGFloat(numberOfSymbols)).isActive = true
            customView.sizeToFit()
            customView.clipsToBounds = true
            customView.backgroundColor = .clear
            stackView.addArrangedSubview(customView)
        }
        stackView.alpha = 0.0
        return stackView
    }
    
    
    @objc func handleTap(_ sender : UITapGestureRecognizer? = nil) {
        
        if let senderView = sender , let view = senderView.view {
            if let index = cardViews.firstIndex(of: view){
                if currentSelectedCards.count == 0 {
                    buttonSelectedCards.forEach { cardInside in
                        cardInside.value.layer.borderColor = UIColor.white.cgColor
                        cardInside.value.layer.borderWidth = 2.0
                        
                    }
                    self.currentSelectedCards.removeAll()
                    self.buttonSelectedCards.removeAll()
                }
                
                if currentSelectedCards.contains(index) {
                    setController.setScore(value: .flipped)
                    view.layer.borderColor = UIColor.white.cgColor
                    view.layer.borderWidth = 2.0
                    currentSelectedCards.remove(at:  currentSelectedCards.firstIndex(of: index)!)
                    buttonSelectedCards[index] = nil
                    return
                } else {
                    view.layer.borderColor = UIColor.black.cgColor
                    view.layer.borderWidth = 2.0
                    buttonSelectedCards[index] = view
                    currentSelectedCards.append(index)
                }
                
                if currentSelectedCards.count == 3 {
                    cardsMatchUp()
                }	 else if currentSelectedCards.count > 3 {
                    buttonSelectedCards.forEach { cardInside in
                        cardInside.value.layer.borderColor = UIColor.white.cgColor
                        cardInside.value.layer.borderWidth = 2.0
                    }
                    self.currentSelectedCards.removeAll()
                }
            }
        }
    }
}
