import UIKit

class CardView: UIView {
    var viewController = SetViewController()

    override init(frame: CGRect) {
          super.init(frame: frame)
       
          self.backgroundColor = UIColor.darkGray
      }
       
      required init?(coder aDecoder: NSCoder) {
          super.init(coder: aDecoder)
      }

    override func draw(_ rect: CGRect) {
        viewController.bounds=self.bounds
        viewController.grid = Grid(layout: .aspectRatio(0.7), frame: self.bounds)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
            self.setNeedsDisplay()
            self.setNeedsLayout()
    }
    
    override func layoutSubviews() {
        viewController.cardViews.enumerated().forEach{ index, cards in
            viewController.reorderAnimation(gridIndex: index)
        }
    }
}
